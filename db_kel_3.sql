/*
SQLyog Professional v12.5.1 (64 bit)
MySQL - 10.4.28-MariaDB : Database - db_kel_3
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_kel_3` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */;

USE `db_kel_3`;

/*Table structure for table `barang` */

DROP TABLE IF EXISTS `barang`;

CREATE TABLE `barang` (
  `kode_barang` char(15) NOT NULL,
  `nama_barang` varchar(255) DEFAULT NULL,
  `jumlah` int(15) DEFAULT NULL,
  PRIMARY KEY (`kode_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `barang` */

insert  into `barang`(`kode_barang`,`nama_barang`,`jumlah`) values 
('0001','Komputer',50),
('00010','Speaker',6),
('0002','Mouse',50),
('0003','LCD Monitor',50),
('0004','Microfon',2),
('0006','Taplak Meja',1),
('0007','Hexause',9),
('0008','Vas Bunga',5),
('0009','hordeng',2),
('0010','TV',1),
('0012','TV',1);

/*Table structure for table `data_pengajuan_detail` */

DROP TABLE IF EXISTS `data_pengajuan_detail`;

CREATE TABLE `data_pengajuan_detail` (
  `kode_pengajuan` char(20) NOT NULL,
  `kode_barang` char(20) NOT NULL,
  `jumlah` int(20) DEFAULT NULL,
  PRIMARY KEY (`kode_pengajuan`,`kode_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `data_pengajuan_detail` */

insert  into `data_pengajuan_detail`(`kode_pengajuan`,`kode_barang`,`jumlah`) values 
('0001','0005',40),
('0002','0003',25),
('0004','0002',30),
('0006','0004',2);

/*Table structure for table `data_pengembalian_detail` */

DROP TABLE IF EXISTS `data_pengembalian_detail`;

CREATE TABLE `data_pengembalian_detail` (
  `kode_pengembalian` char(20) NOT NULL,
  `kode_barang` char(20) NOT NULL,
  `jumlah` int(15) DEFAULT NULL,
  PRIMARY KEY (`kode_pengembalian`,`kode_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `data_pengembalian_detail` */

insert  into `data_pengembalian_detail`(`kode_pengembalian`,`kode_barang`,`jumlah`) values 
('1111','0004',2),
('1112','0002',30),
('1113','0005',40),
('1114','0003',25);

/*Table structure for table `pengajuan` */

DROP TABLE IF EXISTS `pengajuan`;

CREATE TABLE `pengajuan` (
  `kode_pengajuan` char(10) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `npm_peminjam` int(20) DEFAULT NULL,
  `nama_peminjam` varchar(255) DEFAULT NULL,
  `prodi` varchar(255) DEFAULT NULL,
  `no_handphone` int(20) DEFAULT NULL,
  PRIMARY KEY (`kode_pengajuan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `pengajuan` */

insert  into `pengajuan`(`kode_pengajuan`,`tanggal`,`npm_peminjam`,`nama_peminjam`,`prodi`,`no_handphone`) values 
('00010','2023-06-15',20311455,'selviana','Sistem Informasi',2147483647),
('0002','2023-06-05',20311232,'jenny','Sastra Inggis',2147483647),
('0003','2023-06-06',20311070,'salsa','Teknik Sipil',2147483647),
('0004','2023-06-08',20311456,'nisa','Teknik Elektro',2147483647),
('0005','2023-06-09',20311058,'urip','Manajemen',2147483647),
('0006','2023-06-10',20311072,'anggi','akuntansi',2147483647),
('0007','2023-06-11',20311061,'rapli','Pendidikan Olahraga',2147483647),
('0008','2023-06-12',20311456,'abizar','Matematika',2147483647),
('0009','2023-06-14',20311523,'juan','Teknik Informatika',2147483647),
('0011','2023-08-07',20311096,'samuel','SI',99908),
('0012','2023-07-01',20311096,'samuel','SI',9999);

/*Table structure for table `pengembalian` */

DROP TABLE IF EXISTS `pengembalian`;

CREATE TABLE `pengembalian` (
  `kode_pengembalian` char(15) NOT NULL,
  `kode_pengajuan` char(15) NOT NULL,
  `tanggal_kembali` date DEFAULT NULL,
  PRIMARY KEY (`kode_pengembalian`,`kode_pengajuan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `pengembalian` */

insert  into `pengembalian`(`kode_pengembalian`,`kode_pengajuan`,`tanggal_kembali`) values 
('1112','0004','2023-06-04'),
('1113','0001','2023-06-09'),
('1114','0002','2023-06-05');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `user` */

insert  into `user`(`id_user`,`name`,`username`,`pass`,`role`) values 
(1,'lolo','gege','12345','antap');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
